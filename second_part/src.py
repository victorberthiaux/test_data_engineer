
class MySet(set):
    def __init__(self, l):                      
        self.value = set(l)
    def __add__(self, oset):                    
        return set(self.value | oset.value)
    def __eq__(self, oset):
        return self.value == oset.value
    def __sub__(self, oset):
        return set(self.value - oset.value)

def decorator_check_max_int(func):
    def wrap(*args):                    #wrap into checker
        if func(*args) > maxsize:
            return maxsize
        else:
            return func(*args)
    return wrap


@decorator_check_max_int
def add(a, b):
    return a + b

def ignore_exception(exception):
    def decfunc(func):                  #1rst decorator get exception type we want to ignore
        def wrap(*args):
            try:                        #2nd decorator tries functions and catches exception if it's the one we were looking for
                return func(*args)
            except exception:
                return None
        return wrap
    return decfunc


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList(type):
    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, (list, Ex), dct)     #our class will derivate from list type and the Ex class
        return x

class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

