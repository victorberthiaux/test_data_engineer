def exercise_one():
    for  i in range(1, 101):
        if (i%3 == 0) and (i%5 == 0):
            print("ThreeFive")
        elif (i%5 == 0):
            print("Five")
        elif (i%3) == 0:
            print("Three")
        else:
            print(i)


def find_missing_nb(arr):
    t = ((len(arr) + 1) * (len(arr) + 2)) / 2   #compute the sum of all the numbers within the array if there was no missing number
    r = 0
    for i in arr: r+=i                          #iterate over array to get the actual sum of the numbers inside it
    return int(t - r)                           #return the difference

def bblsort(arr):                               #bubble sort that skips negative numbers
    if (len(arr) < 2):
        return arr
    cur = 0
    temp = 0
    for j in range(len(arr) + 1):
        for i in range(0, len(arr) - 1):
            while (i + 1 < len(arr) and arr[i] < 0):    #reach first positive nbr or last number within array
                i+=1
            if arr[i] >= 0:                             #if number reached is positive, proceed as usual bblsort
                cur = i
                if i + 1 < len(arr):
                    i+=1
            while (i + 1 < len(arr) and arr[i] < 0):    #reach first positive nbr or last number within array
                i+=1
            if (arr[i] >= 0 and arr[i] < arr[cur]):     #if found 2 positives, compare them and swap if needed
                temp = arr[cur]
                arr[cur] = arr[i]
                arr[i] = temp
    return arr



