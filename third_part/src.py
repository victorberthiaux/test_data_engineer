import pandas as pd

product_list = pd.read_json('data/products.json')

def clean_exp(w):
    if isinstance(w, str):
        return w.replace('NULL', '')
    return(w)

def clean_exp_coma(w):
    if isinstance(w, str):
        return w.replace(',NULL', '')
    return(w)

def clean_cat(prods):
    for element in prods:
        prods[element] = prods[element].apply(clean_exp_coma)   #Clear every ",NULL"
        prods[element] = prods[element].apply(clean_exp)        #Clear if there is an entry at is only "NULL"
    return prods

d1 = 0
d2 = 0
assets = 0

def import_raw_data():
    global d1, d2, assets

    d1 = pd.read_csv('data/17-10-2018.3880', sep=";", header=0)
    d2 = pd.read_csv('data/18-10-2018.3880', sep=";", header=0)
    assets = pd.read_csv('data/back_office.csv', sep=",", header=0)

def forcestr(o):
    return str(o)

def process_data():
    global d1, d2, assets

    assets["pe_ref_in_enseigne"] = assets["pe_ref_in_enseigne"].apply(forcestr)     #every product ID is not an int, so we
    d1["identifiantproduit"] = d1["identifiantproduit"].apply(forcestr)             #cast to str to be able to merge
    d2["identifiantproduit"] = d2["identifiantproduit"].apply(forcestr)

    merged1 = pd.merge(
    d1.rename(columns={"prixproduit" : "prixproduit_17"}),      #rename the prices column to be able to
    d2.rename(columns={"prixproduit" : "prixproduit_18"}),      #distinguish them, but we do not modify original DFs
    how="left",
    on="identifiantproduit",                                    #we join by product ID
    left_on=None,
    right_on=None,
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
    indicator=False,
    validate=None,
    )

    merged2 = pd.merge(
    merged1,
    assets.rename(columns={"pe_ref_in_enseigne" : "identifiantproduit"}),   #rename the product ID column so there is no
    how="left",                                                             #redundancy within the columns
    on="identifiantproduit",
    left_on=None,
    right_on=None,
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
    indicator=False,
    validate=None,
    )

def average_prices():
    merged = process_data()
    data = [merged["identifiantproduit"], merged[["prixproduit_17", "prixproduit_18"]].mean(axis=1)]

    headers = ["id", "price_average"]

    df = pd.concat(data, axis=1, keys=headers) #creating new DF for just prodID and price average so it's easier to read
    df.to_csv("price_average", index=False)

def d1merge():
    global d1, assets

    assets["pe_ref_in_enseigne"] = assets["pe_ref_in_enseigne"].apply(forcestr)
    d1["identifiantproduit"] = d1["identifiantproduit"].apply(forcestr)

    merged = pd.merge(
    d1,
    assets.rename(columns={"pe_ref_in_enseigne" : "identifiantproduit"}),
    how="left",
    on="identifiantproduit",
    left_on=None,
    right_on=None,
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
    indicator=False,
    validate=None,
    )

    return merged

def d2merge():
    global d2, assets

    assets["pe_ref_in_enseigne"] = assets["pe_ref_in_enseigne"].apply(forcestr)
    d2["identifiantproduit"] = d2["identifiantproduit"].apply(forcestr)

    merged = pd.merge(
    d2,
    assets.rename(columns={"pe_ref_in_enseigne" : "identifiantproduit"}),
    how="left",
    on="identifiantproduit",
    left_on=None,
    right_on=None,
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
    indicator=False,
    validate=None,
    )

    return merged


def count_products_by_categories_by_day():
    d1ret={}    #d1ret : dict. of retailers categories for d1
    d2ret={}    #d2ret : dict. of retailers categories for d2
    d1di={}     #d1di : dict of DataImpact's categories for d1
    d2di={}     #d2di : dict of DataImpact's categories for d2
    day1 = d1merge()    #merge data from day 1 to assets
    for ret, di in zip(day1["categorieenseigne"].tolist(), day1["p_id_cat"].tolist()): #make a dictionary with each unique cat. for d1
        if ret not in d1ret.keys():
            d1ret[ret] = 1
        else:
            d1ret[ret] += 1

        if di not in d1di.keys():
            d1di[di] = 1
        else:
            d1di[di] += 1

    day2 = d2merge() #merge data from day 2 to assets
    for ret, di in zip(day2["categorieenseigne"].tolist(), day2["p_id_cat"].tolist()): #make a dictionary with each unique cat. for d2
        if ret not in d2ret.keys():
            d2ret[ret] = 1
        else:
            d2ret[ret] += 1

        if di not in d2di.keys():
            d2di[di] = 1
        else:
            d2di[di] += 1

    return ((d1ret, d1di), (d2ret, d2di)) #First tupple is d1 data, second tupple is d2 data


def average_products_by_categories():
    (day1, day2) = count_products_by_categories_by_day()
    d1ret = 0
    d2ret = 0
    d1di = 0
    d2di = 0

    for val in day1[0].values(): #average values in each dictionary obtained though previous function
        d1ret+= val
    d1ret /= len(day1[0])

    for val in day1[1].values():
        d1di+= val
    d1di /= len(day1[1])

    for val in day2[0].values():
        d2ret+= val
    d2ret /= len(day2[0])

    for val in day2[1].values():
        d2di+= val
    d2di /= len(day1[1])

    return ((d1ret, d1di), (d2ret, d2di)) #same format as in count_products_by_categories_by_day


if __name__ == '__main__':
    raw_data_20181017 = import_raw_data()
    raw_data_20181018 = import_raw_data()

    processed_data_20181017 = process_data()
    processed_data_20181018 = process_data()
    ...
